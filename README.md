Interface Based


toRoman(int n)

	int n 

		Characteristic: 
			Relation with 0
		Partition: 
			<0
			=0
			>0

fromRoman(string s)
	
	String s

		Characteristic: 
			Nullness
		Partition: 
			t
			f

		Characteristic: 
			Relation of length with 0
		Partition:
			=0
			>0

		Characteristic:
			Case
		Partition:
			Uper
			Lower
			Mixed

		Characteristic: 
			Content
		Partition:
			valid Roman nr
			invalid roman nr
			