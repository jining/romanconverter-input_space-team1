import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import roman.RomanConverter;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class InterfaceBasedTestSuite {

    private static RomanConverter romanConverter = new RomanConverter();

    //FromRoman

    @Test
    public void shouldHandle0LengthString() {
        romanConverter.fromRoman("");
    }

    @Test
    public void shouldHandleAllCases(){
        romanConverter.fromRoman("iV");
        romanConverter.fromRoman("i");
        romanConverter.fromRoman("V");
    }

    @Test(expected = Exception.class)
    public void shouldThrowNullPointer_onNullString_fromRoman() {
        romanConverter.fromRoman(null);
    }

    @Test
    public void shouldHandleCorrectInput(){
        romanConverter.fromRoman("IV");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionOnWrongLanguage(){
        romanConverter.fromRoman("漢");
        romanConverter.fromRoman("Д");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionOnWrongEncoding(){
        String input = "A" + "\u00ea" + "\u00f1" + "\u00fc" + "C";
        romanConverter.fromRoman(input);
        romanConverter.fromRoman("© ");
    }

    //ToRoman


    @Test
    public void shouldNotCrashOn0(){
        //Since you can convert empty string to 0, makes sense for the reverse to work.
        romanConverter.toRoman(0);
    }

    @Test
    public void shouldNotCrashOnPositive(){
        romanConverter.toRoman(1);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionOnNegativeInput(){
        romanConverter.toRoman(-1);
    }


}
