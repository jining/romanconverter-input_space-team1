import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import roman.RomanConverter;
import static org.junit.Assert.assertEquals;



@RunWith(JUnit4.class)
public class FunctionalityBasedTestSuite {

    private static RomanConverter romanConverter = new RomanConverter();
    
    @Test
    public void inputBetween1and3999_toRoman(){
    	assertEquals("I", romanConverter.toRoman(1));
    	
    	
    }
    @Test(expected = Exception.class)
    public void inputOutside1and3999_toRoman(){
    	romanConverter.toRoman(4000);
    }
    
    @Test
    public void toFrominputBetween1and3999_toRoman(){
    	assertEquals(1, romanConverter.fromRoman(romanConverter.toRoman(1)));
    	
    	
    }
    @Test(expected = Exception.class)
    public void toFrominputOutside1and3999_toRoman(){
    	romanConverter.fromRoman(romanConverter.toRoman(4000));
    }
    @Test
    public void inputinteger_toRoman(){
    	assertEquals("II", romanConverter.toRoman(2));
    }

    @Test(expected = Exception.class)
    public void inputNoninteger_toRoman(){
    	romanConverter.toRoman(2.5);
    }
    @Test
    public void inputValidRoman_fromRoman(){
    	assertEquals(1, romanConverter.fromRoman("I"));
    	    	
    }

    @Test(expected = Exception.class)
    public void inputinValidRoman_fromRoman(){
    	romanConverter.fromRoman("OS");

    }

    @Test
    public void inputUppercase_fromRoman(){
    	assertEquals(2, romanConverter.fromRoman("II"));
    }

    @Test(expected = Exception.class)
    public void inputLowercase_fromRoman(){
    	romanConverter.fromRoman("ii");
    }
    @Test(expected = Exception.class)
    public void inputMixedcase_fromRoman(){
    	romanConverter.fromRoman("Ii");
    }
    
}